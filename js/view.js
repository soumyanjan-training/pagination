import * as model from "./model.js";
import * as controller from "./controller.js";

export const footer = document.querySelector(".Pagination");
export let pages;

const name = {
  sortState: "Ascending"
};
const email = {
  sortState: "Ascending"
};
const id = {
  sortState: "Descending"
};

function initSortState() {
  name.sortState = "Ascending";
  email.sortState = "Ascending";
  id.sortState = "Descending";
}

const DataSection = document.querySelector(".data");
const spinner = document.querySelector(".spinner");
const tbody = document.querySelector(".tbody");
let param = null;
const pagination = function(data) {
  footer.innerHTML = "";
  pages = Math.ceil(data.length / 5);
  if (pages > 0) {
    footer.insertAdjacentHTML(
      "beforeend",
      `<button class = "footer-btn mov" id="<<" >${"<<"}</button>`
    );
    for (let i = 1; i <= pages; i++) {
      const tHtml = `<button class = "footer-btn" id="${i}">${i}</button>`;
      footer.insertAdjacentHTML("beforeend", tHtml);
    }
    footer.insertAdjacentHTML(
      "beforeend",
      `<button class = "footer-btn mov" id=">>" >${">>"}</button>`
    );
  }
};
export const viewData = function(data, pageNo) {
  const tableData = document.querySelector(".tbody");
  tableData.innerHTML = "";
  const startIndex = (pageNo - 1) * 5;
  const lastIndex = startIndex + 5;
  const data1 = data.slice(startIndex, lastIndex);

  for (let i = 0; i < data1.length; i++) {
    const html = `<tr>
      <td class="id">${data1[i].id}</td>
      <td class="name">${data1[i].name}</td>
      <td class="email">${data1[i].email}</td>
    </tr>
`;
    tableData.insertAdjacentHTML("beforeend", html);
  }
  if (param) sortHighLight(param);
  pagination(data);
  document
    .querySelectorAll(".footer-btn")
    .forEach(el => el.classList.remove("active"));
  document.getElementById(pageNo).classList.add("active");
  document.getElementById("<<").classList.add("hidden");
};
export const addSpinner = function() {
  tbody.innerHTML = "";
  const spin =
    '<tr><td></td><td><div class="spinner"><span class="iconify" data-icon="icomoon-free:spinner"></span></div></td><td></td></tr>';
  tbody.insertAdjacentHTML("beforeend", spin);
};
export const removeSpinner = function() {
  tbody.innerHTML = "";
};

export const SearchDisplayPage = async function(Name) {
  const data = await model.getFullData();
  const dataFiltered = data.filter(data => {
    if (data.name.toLowerCase().includes(Name.toLowerCase())) return data;
  });
  console.log(dataFiltered);
  if (!(dataFiltered.length === 0)) {
    viewData(dataFiltered, 1);
    return dataFiltered;
    initSortState();
  } else {
    alert("no data found");
  }
};

export const SortedDisplayPage = async function(parameter) {
  try {
    addSpinner();
    const data = controller.data;

    if (parameter == "name") param = name;
    if (parameter == "email") param = email;
    if (parameter == "id") param = id;
    const sortState = param.sortState;
    model.sortData(data, parameter, sortState);
    if (sortState === "Ascending") param.sortState = "Descending";
    else param.sortState = "Ascending";
    removeSpinner();
    viewData(data, 1);
  } catch (err) {
    console.log(err.message);
  }
};

export function sortHighLight(parameter) {
  const nameRow = document.querySelector(".name");
  const idRow = document.querySelector(".id");
  const emailRow = document.querySelector(".email");
  nameRow.classList.remove("highlighted");
  idRow.classList.remove("highlighted");
  emailRow.classList.remove("highlighted");
  if (parameter == id) idRow.classList.add("highlighted");
  if (parameter == name) nameRow.classList.add("highlighted");
  if (parameter == email) emailRow.classList.add("highlighted");
}
