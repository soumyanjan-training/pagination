import * as view from "./view.js";
import * as model from "./model.js";

const tableData = document.querySelector(".tbody");
const sortBtn = document.querySelector(".Sort");
const table = document.querySelector(".table");
const inputName = document.querySelector(".Name");
const refresh = document.querySelector(".refresh");

export let data;
let curPageNo;
init();
attachEventListener();

async function init() {
  view.addSpinner();
  data = await model.getFullData();
  curPageNo = 1;
  view.removeSpinner();
  view.viewData(data, curPageNo);
}

function attachEventListener() {
  view.footer.addEventListener("click", function(e) {
    const pageD = e.target.textContent;
    if (pageD === ">>") {
      curPageNo++;
      view.viewData(data, curPageNo);
    }
    if (pageD === "<<") {
      curPageNo--;
      view.viewData(data, curPageNo);
    }
    const pageNo = Number(pageD);
    if (!(isNaN(pageNo) || pageNo === curPageNo)) {
      curPageNo = pageNo;
      view.viewData(data, curPageNo);
    }
    if (curPageNo === view.pages)
      document.getElementById(">>").classList.add("hidden");
    else document.getElementById(">>").classList.remove("hidden");

    if (curPageNo === 1) document.getElementById("<<").classList.add("hidden");
    else document.getElementById("<<").classList.remove("hidden");
  });

  window.addEventListener("submit", async function(e) {
    e.preventDefault();
    if (!inputName.value) {
      alert("input is empty");
    } else {
      data = await view.SearchDisplayPage(inputName.value);
      curPageNo = 1;
    }
  });

  sortBtn.addEventListener("click", async function(e) {
    console.log(e.target.parentElement);
    if (
      e.target.textContent.startsWith("Name") ||
      e.target.parentElement.classList.contains("name")
    ) {
      await view.SortedDisplayPage("name");
    }
    if (
      e.target.textContent.startsWith("Email") ||
      e.target.parentElement.classList.contains("email")
    ) {
      await view.SortedDisplayPage("email");
    }
    if (
      e.target.textContent.startsWith("Id") ||
      e.target.parentElement.classList.contains("id")
    ) {
      await view.SortedDisplayPage("id");
    }
    curPageNo = 1;
  });

  refresh.addEventListener("click", init);
}
